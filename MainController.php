<?php

require_once 'MyLogger.php';
require_once 'PDOAdapter.php';

class MainController
{

    public function maxAge()
    {
        $maxAge = $this->getMaxAge();
        echo $maxAge;
    }

    /**
     * @return mixed
     */
    private function getMaxAge()
    {
        $myLogger = new MyLogger('file.txt');
        $pdo = new PDOAdapter('mysql:host=localhost', 'root', 'root', $myLogger);
        $sql = 'SELECT age FROM test.person ORDER BY age DESC LIMIT 1';

        return $pdo->execute('selectOne', $sql)->age;
    }

    /**
     * @return mixed
     */
    private function findPersonFirstName()
    {
        $maxAge = $this->getMaxAge();

        $myLogger = new MyLogger('file.txt');
        $pdo = new PDOAdapter('mysql:host=localhost', 'root', 'root', $myLogger);
        $sql = "SELECT firstname FROM test.person WHERE mother_id IS NULL AND  age < :age LIMIT 1";
        $statement = $pdo->prepare($sql);
        $statement->bindParam('age', $maxAge);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_OBJ);

        return $result;
    }

    /**
     * @return string
     */
    public function changeAge()
    {
        $personWithoutMotherId = $this->findPersonFirstName();
        $maxAge = $this->getMaxAge();

        $myLogger = new MyLogger('file.txt');
        $pdo = new PDOAdapter('mysql:host=localhost', 'root', 'root', $myLogger);
        $sql = 'UPDATE test.person SET age =:age WHERE firstname =:firstname';
        $statement = $pdo->prepare($sql);
        $statement->bindParam('age', $maxAge);
        $statement->bindParam('firstname', $personWithoutMotherId->firstname);
        $statement->execute();
        $getLastUpdate = $pdo->getLastInsertId();

        return $getLastUpdate;
    }

    /**
     * @return array
     */
    public function getAgeList()
    {
        $myLogger = new MyLogger('file.txt');
        $pdo = new PDOAdapter('mysql:host=localhost', 'root', 'root', $myLogger);
        $sql = 'SELECT lastname, firstname, age FROM test.person WHERE age = (SELECT MAX(age) from test.person) ORDER BY lastname';
        $statement = $pdo->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

}