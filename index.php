<?php

require_once 'MainController.php';
$var = (new MainController)->getAgeList();

(new MainController())->changeAge();
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta charset="utf-8"/>
    <title>Task</title>
</head>
<body>


<main role="main" class="flex-shrink-0">
    <div class="container">
        <h1 class="mt-5">Список персон максимального возраста(<?php (new MainController())->maxAge();?>)</h1>
        <table class="table table-striped">
            <thead class="thead-dark">
            <tr>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Возраст</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($var as $row): ?>
             <tr>
                <td><?php echo $row['lastname']; ?></td>
                <td><?php echo $row['firstname']; ?></td>
                <td><?php echo $row['age']; ?></td>
             </tr>
            <?php  endforeach; ?>
            </tbody>
        </table>
    </div>
</main>
<footer class="footer mt-auto py-3">
    <div class="container">
        <span class="text-muted"><a href="https://gitlab.com/moryak/inlinetesttask">https://gitlab.com/moryak/inlinetesttask</a></span>
    </div>
</footer>

</body>
</html>
